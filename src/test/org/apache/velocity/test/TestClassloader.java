package org.apache.velocity.test;

import java.io.File;
import java.io.FileInputStream;

/**
 *  Simple (real simple...) classloader that depends
 *  on a Foo.class being located in the classloader
 *  directory under test
 */
public class TestClassloader extends ClassLoader
{
    private final static String testclass =
        "test/classloader/Foo.class";

    private Class fooClass = null;

    public TestClassloader()
            throws Exception
    {
        File f = new File( testclass );

        byte[] barr = new byte[ (int) f.length() ];

        FileInputStream fis = new FileInputStream( f );
        fis.read( barr );
        fis.close();

        fooClass = defineClass("Foo", barr, 0, barr.length);
    }


    public Class findClass(String name)
    {
        return fooClass;
    }
}

